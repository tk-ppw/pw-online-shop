from django.contrib import admin
from .models import coupon_data

admin.site.register(coupon_data)
class coupon_admin(admin.ModelAdmin):
    list_display = ('name', 'description')

