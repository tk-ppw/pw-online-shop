from django import forms 
from coupon.models import coupon_data

coupon_search_choice =(
    ("Discount","Discount"),
    ("Code", "Code")
    )

class OptionCouponForm(forms.Form):
     search_option = forms.ChoiceField(label = '',  widget = forms.Select(attrs = {'class' : 'form-control'},choices = coupon_search_choice))

class SearchCouponForm(forms.Form):
     search_field = forms.CharField(
          label = '', max_length=15,\
                widget=forms.TextInput(attrs={'autocomplete':'off',\
                     'id':'search_field'}))
    