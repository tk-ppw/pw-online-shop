from django.test import TestCase, Client, override_settings
from .models import *
from .views import *
from datetime import datetime
from django.utils.timezone import make_aware
@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class QuestionModelTest(TestCase):
    def test_coupon_page(self):
        response = Client().get('/coupon/')
        self.assertEqual(response.status_code, 200)
        
    def setUp(self):
        coupon_data.objects.create( coupon_code = code,
        discount_percentage=10)     
    def test_object_coupon_if_exist(self):
        coupon1 = coupon_data.objects.all().count()
        self.assertLess(0, coupon1)
    def test_object_coupon_expired_date(self):

        #if exist
        coupon1 = coupon_data.objects.all()[0].expired_date
        self.assertIsInstance(coupon1, datetime)

        #if more than now
        now = make_aware(datetime.now())
        self.assertLess(now, coupon1)

        #if less than now
        now = make_aware(datetime.now()+timedelta(days=1))
        self.assertLess(coupon1, now)

        #if views delete the coupon
        coupon_test = coupon_data.objects.create(expired_date=make_aware(datetime.now()-timedelta(days=1)))
        coupon_test.save()
        dummy = Client()
        response = dummy.get('/coupon/')
        self.assertEqual(1, coupon_data.objects.all().count())

    def test_printing_objects(self):
        self.assertIsInstance(str(coupon_data.objects.all()[0]),str ) 

    def test_views_working(self):
        dummy = Client()
        coupon_data.objects.all().delete()

        #Checking if the money got dot('.')
        coupon_data.objects.create(minimum_price_in_rupiah_multiplied_by_1000=100)
        coupon_test = coupon_data.objects.all()[0].minimum_price_in_rupiah_multiplied_by_1000
        response = dummy.get('/coupon/')
        self.assertContains(response, '100.000', status_code=200)
        self.assertTemplateUsed(response, 'coupon_page.html')
        coupon_data.objects.all().delete()
        
        #checking if request no exist
        coupon_data.objects.create(discount_percentage=20)
        response = dummy.get('/coupon/', {'search_option':'Discount','search_field':'tes'})
        self.assertContains(response,':(', status_code=200)

        #checking if "request.GET == Discount" works
        response = dummy.get('/coupon/', {'search_option':'Discount','search_field':'10'})
        self.assertContains(response, 'DISKON', status_code = 200)

        #checking if "request.get == Code" works
        response = dummy.get('/coupon/', {'search_option':\
            'Code','search_field':coupon_data.objects.all()[0].coupon_code})
        self.assertContains(response, coupon_data.objects.all()[0].coupon_code, status_code = 200)
        #if the coupon code doesn't exist
        response = dummy.get('/coupon/', {'search_option':\
            'Code','search_field':'tes'})
        self.assertContains(response, ':(', status_code=200)
        
