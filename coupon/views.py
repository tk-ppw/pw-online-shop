from django.shortcuts import render
from .models import code, coupon_data
from .forms import *
from datetime import datetime
from django.utils.timezone import make_aware
import pytz
import json

def expired_date_function(coupon_object):
    list_price = []
    list_hour = []
    list_minute = []
    list_day = []
    list_month = []
    list_year = []
    list_discount = []

    for i in coupon_object:
        price = i.minimum_price_in_rupiah_multiplied_by_1000 * 1000
        price = str(price).strip()
        give_dot = []
        for a in price:
            give_dot.append(a)
        length = len(give_dot)
        for j in range(length - 3, -1, -3):
            if j <= 0 :
                break
            give_dot.insert(j, '.')
        price = ''.join(give_dot)
        list_price.append(price)

        coupon_minute = i.expired_date.minute
        if coupon_minute < 10:
            coupon_minute = '0' + str(coupon_minute)
        list_minute.append(str(coupon_minute))

        coupon_hour = i.expired_date.hour + 7
        if coupon_hour < 10:
            coupon_hour = '0' + str(coupon_hour)
        list_hour.append(str(coupon_hour))

        coupon_day = i.expired_date.day
        if coupon_day < 10:
            coupon_day = '0' + str(coupon_day)
        list_day.append(str(coupon_day))

        coupon_month = i.expired_date.month
        if coupon_month < 10:
            coupon_month = '0' + str(coupon_month)
        list_month.append(str(coupon_month))

        coupon_year = i.expired_date.year
        list_year.append(str(coupon_year))

    return list_price, list_hour, list_minute, list_day, \
        list_month, list_year, list_discount

def find_discount(search_form,\
                 option_form, coupon_object, request,\
                value_coupon, list_price, list_hour,\
                list_minute, list_day, list_month, \
                    list_year, list_discount):
    try: 
        list1 = []
        for coupon in coupon_object:
            if coupon.discount_percentage >= int(value_coupon):
                list1.append(coupon)
    except:
        context = {'search_form': search_form, 'option_form':option_form }
        return render(request, 'coupon_page.html', context)
    coupon_object = list1
    coupon_deploy = zip(coupon_object, list_price, list_hour, list_minute,\
        list_day, list_month, list_year) 
    context = {'coupon_deploy': coupon_deploy, 'search_form': search_form, 'option_form':option_form, }
    return render(request, 'coupon_page.html', context)

def find_code(search_form,\
                 option_form, coupon_object, request,\
                value_coupon, list_price, list_hour,\
                list_minute, list_day, list_month, \
                    list_year, list_discount):
    list1 = []
    for coupon in coupon_object:
        if coupon.coupon_code == value_coupon:
            list1.append(coupon)
    coupon_object = list1
    if list1 == []:
        context = {'search_form': search_form, 'option_form':option_form }
        return render(request, 'coupon_page.html', context)
    coupon_deploy = zip(coupon_object, list_price, list_hour, list_minute,\
        list_day, list_month, list_year)
    context = {'coupon_deploy': coupon_deploy, 'search_form': search_form, 'option_form':option_form, }
    return render(request, 'coupon_page.html', context)

def get_data(coupon_object):
    dict1 = {"code": [], "discount": []}
    for obj in coupon_object:
        dict1["code"].append(obj.coupon_code)
        dict1["discount"].append(obj.discount_percentage)
    return json.dumps(dict1)

def coupon_page(request):
    coupon_object = coupon_data.objects.all()

    for coupon in coupon_object:
        if coupon.expired_date < make_aware(datetime.now(),  timezone=pytz.timezone("Asia/Jakarta")):
            coupon.delete()

    option_form = OptionCouponForm(request.GET or None)
    search_form = SearchCouponForm(request.GET or None)
    
    list_price, list_hour, list_minute, list_day, \
            list_month, list_year, list_discount = expired_date_function(coupon_object)

    if request.GET:
        find_coupon = request.GET['search_option']
        value_coupon = request.GET['search_field']

        if find_coupon == 'Discount':
            return find_discount(search_form,\
                 option_form, coupon_object, request,\
                value_coupon, list_price, list_hour,\
                list_minute, list_day, list_month, \
                    list_year, list_discount)
        if find_coupon == 'Code':
            return find_code(search_form,\
                 option_form, coupon_object, request,\
                value_coupon, list_price, list_hour,\
                list_minute, list_day, list_month, \
                    list_year, list_discount)

    json_data = get_data(coupon_object)
    print(json_data)
    coupon_deploy = zip(coupon_object, list_price, list_hour, list_minute,list_day, list_month, list_year) 
    context = {'coupon_deploy': coupon_deploy, 'search_form': search_form, 'option_form':option_form,\
        'json_data': json_data }
    return render(request, 'coupon_page.html', context)