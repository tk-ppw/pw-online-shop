from django import forms
from django.forms import ModelForm
from .models import Transaksi,Barang
from django.core.validators import MinValueValidator

coupon_search_choice =(
    ("Discount","Discount"),
    ("Code", "Code")
    )

class TransaksiForm(forms.Form):
    pembeli = forms.CharField(
        label = 'Nama Pembeli',
        required=True,
        widget = forms.TextInput(attrs = {'class' : 'form-control form-control-sm'})
    )
    quantity = forms.IntegerField(
        label = "Qty",
        required = True,
        widget = forms.TextInput(attrs = {'class' : 'col-auto form-control form-control-sm'}),
        validators = [MinValueValidator(1)]
    )
    
    class Meta:
        fields = ('pembeli','quantity')

class CouponForm(forms.Form):
    coupon = forms.CharField(
        label = "",
        widget = forms.TextInput(attrs = {'class' : 'col-auto form-control form-control-sm', 
        'placeholder' : 'if not use, type "not use"', 'autocomplete':'off',
        'id':'search_field'})
    )
    class Meta:
        fields = ('coupon')

class OptionCouponForm(forms.Form):
     search_option = forms.ChoiceField(label = '', 
     choices = coupon_search_choice,
     )
