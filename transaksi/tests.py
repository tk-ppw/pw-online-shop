from django.test import TestCase,Client,override_settings,LiveServerTestCase
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from django.http import request
from django.contrib.auth.models import User
import time

from catalogues.views import Barang
from coupon.models import coupon_data
from .views import histori_transaksi,transaksi,checkout,generate
from .models import Transaksi
from .forms import TransaksiForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class TestTrf(TestCase): 
    # Per Transaction Test
    def test_url_transaction(self):
        barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        Transaksi.objects.create(pembeli = "Apis", barang = "Build Burning Gundam", kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
        aidi = Transaksi.objects.get(barang = "Build Burning Gundam")
        response = Client().get('/transaksi/' + str(aidi.id) + "/")
        self.assertEqual(response.status_code, 200)

    # def test_url_post_transaction(self):
    #     barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
    #     Transaksi.objects.create(pembeli = "Apis", barang = "Build Burning Gundam", kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
    #     aidi = Transaksi.objects.get(barang = "Build Burning Gundam")
    #     response = Client().post('/transaksi/' + str(aidi.id) + "/")
    #     self.assertEqual(response.status_code, 200)

    def test_func_per_transaction(self):
        barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        Transaksi.objects.create(pembeli = "Apis", barang = "Build Burning Gundam", kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
        aidi = Transaksi.objects.get(barang = "Build Burning Gundam")
        found = resolve('/transaksi/' + str(aidi.id) + '/')
        self.assertEqual(found.func, transaksi)

    def test_template_per_transaction(self):
        barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        Transaksi.objects.create(pembeli = "Apis", barang = "Build Burning Gundam", kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
        aidi = Transaksi.objects.get(barang = "Build Burning Gundam")
        response = Client().get('/transaksi/' + str(aidi.id) + '/')
        self.assertTemplateUsed(response, 'transaksi.html')

    def test_Aktivitas_forms(self):
        form = TransaksiForm()
        form_data = {'pembeli': 'Calme','quantity': 1, 'coupon':"XXXX"}
        form = TransaksiForm(data=form_data)
        self.assertTrue(form.is_valid())

    # Recent Transaction Test
    def test_transaction_page_using_view_func(self):
        found = resolve('/transaksi/histori/')
        self.assertEqual(found.func, histori_transaksi)

    def test_template(self):
        response = self.client.get(reverse('recent-trans'))
        self.assertTemplateUsed(response, 'histori.html')

    def test_object_transaction_if_exist(self):
        duar = Transaksi.objects.all().count()
        self.assertEqual(0, duar)

    def test_alldb_histori(self):
        prev = Transaksi.objects.all().count()
        barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        Transaksi.objects.create(pembeli = "Apis", barang = barang1.nama, kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
        current = Transaksi.objects.all().count()
        self.assertEqual(current, prev+1)

    def test_cek_db_context(self):
        barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        duar = Transaksi.objects.create(pembeli = "Apis", barang = barang1.nama, kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
        prev = Transaksi.objects.all()
        self.assertIsInstance(duar, Transaksi)

    # ijin nambahin dhi
    def test_transaksi_post_actual_data(self):
        barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        # Transaksi.objects.create(pembeli = "Apis", barang = "Build Burning Gundam", kupon = 'XX', total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
        response = self.client.post('/transaksi/1/', follow=True, data={
                        "pembeli": "badibos",
                        "quantity": 1,
                        "coupon": "not use"
                    })

        # self.assertRedirects(response, '/auth/welcome/badibos', status_code=301,
        #                      target_status_code=301, fetch_redirect_response=True)
        self.assertContains(response, 'badibos')

    # ERROR NOT FOUND TEST
    def test_erro_random_and_render_the_result(self):
        response_post = self.client.post(
            '/transaksi/sda', {
                'guest': 'badingsyalala'
            }
        )
        self.assertEqual(response_post.status_code, 404)

class TestTK2(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')

    def test_url_checkout(self):
        self.client.login(username="john",password="johnpassword")
        response = self.client.get('/transaksi/checkout/')
        self.assertEqual(response.status_code, 200)

    def test_func_checkout(self):
        found = resolve('/transaksi/checkout/')
        self.assertEqual(found.func, checkout)

    def test_template_checkout(self):
        self.client.login(username="john",password="johnpassword")
        response = self.client.get('/transaksi/checkout/')
        self.assertTemplateUsed(response, 'checkout.html')

    #test dummy
    def test_url_generate(self): 
        response = Client().get('/transaksi/generate/')
        self.assertEqual(response.status_code, 200)
    
    def test_func_generate(self):
        found = resolve('/transaksi/generate/')
        self.assertEqual(found.func, generate)

class TransactionFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(
            chrome_options=chrome_options, executable_path='./chromedriver')
        # self.browser = webdriver.Chrome(chrome_options = chrome_options)
        super(TransactionFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def TestAsyncCheckout(self):
        # bikin akun baru gan
        self.browser.get(self.live_server_url + '/auth/signup/')

        self.browser.find_element_by_id("id_username").send_keys("LukeGaming666")
        time.sleep(5)
        self.browser.find_element_by_id("id_first_name").send_keys("Luke")
        time.sleep(5)
        self.driver.find_element_by_id("id_last_name").send_keys("Skywalker")
        time.sleep(5)
        self.browser.find_element_by_id("id_email").send_keys("ros_bad@disney.com")
        time.sleep(5)
        self.browser.find_element_by_id("id_password1").send_keys("reypalpatine")
        time.sleep(5)
        self.browser.find_element_by_id("id_password2").send_keys("reypalpatine")
        time.sleep(5)
        self.browser.find_element_by_name("signup").click()
        time.sleep(5)
        self.assertIn('login', self.browser.page_source)
        time.sleep(5)

        # Login gan
        self.browser.find_element_by_id("id_username").send_keys("LukeGaming666")
        time.sleep(5)
        self.browser.find_element_by_id("id_password").send_keys("reypalpatine")
        time.sleep(5)
        self.browser.find_element_by_name("login").click()
        time.sleep(5)
        self.assertEquals(self.browser.current_url, self.live_server_url + '/')

        # Test beneran gan

        #open dummy
        self.browser.get(self.live_server_url + '/transaksi/generate/')
        # open checkout from dummy
        self.browser.get(self.live_server_url + '/transaksi/checkout/')
        self.browser.implicitly_wait(10)

        # Cek user apakah bener usernamenya
        logUser = self.browser.find_element_by_id('LoggedName').text
        logged = self.browser.find_element_by_id("user_in_page").text
        self.assertIn(logUser,logged)

        # Kalo ada item out of stok, ada disabled di button finish
        duar = self.browser.find_element_by_id("finish-trans").text
        if ("Out of Stock" in self.browser.page_source):
            self.assertIn("disabled", duar)

        # kupon = coupon_data.objects.create(minimum_price_in_rupiah_multiplied_by_1000 = 50, discount_percentage = 50)
        
