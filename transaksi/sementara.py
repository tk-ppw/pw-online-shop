from django.shortcuts import render, redirect
from .forms import TransaksiForm, CouponForm, OptionCouponForm
from .models import Transaksi
from catalogues.models import Barang
from coupon.models import coupon_data
from django.utils import timezone
from django.utils.timezone import make_aware
from datetime import datetime 
import json 

# Create your views here.
def histori_transaksi(request):
    histori = Transaksi.objects.all().order_by('-id')
    #print(histori)
    db = {
        'trans':histori
    }
    return render(request,'histori.html',context=db)

def get_data(coupon_object):
    dict1 = {"code": [], "discount": []}
    for obj in coupon_object:
        dict1["code"].append(obj.coupon_code)
        dict1["discount"].append(obj.discount_percentage)
    return json.dumps(dict1)

def transaksi(request,id_barang):
    
    my_dict={}
    option_form = OptionCouponForm()
    obj = TransaksiForm()
    coupon_form = CouponForm()
    duar = Barang.objects.get(pk=id_barang)
    disk = coupon_data.objects.all()
    kupon = 0
    cek = False
    if request.method == "POST":
        find_coupon = request.POST['search_option']
        if find_coupon == 'Code':
            form = TransaksiForm(request.POST)
            form_coupon = CouponForm(request.POST)
            price = int(form.data['quantity'])
            kupon = form_coupon.data['coupon']
            for i in disk:
                if kupon == i.coupon_code:
                    cek = True
                    kupon_used = i
            if cek:
                if duar.harga >= 1000*kupon_used.minimum_price_in_rupiah_multiplied_by_1000:
                    price = (1 - kupon_used.discount_percentage/100)*int(form.data['quantity'])*duar.harga
                    if kupon_used.expired_date > make_aware(datetime.now()):
                        price = (1 - kupon_used.discount_percentage/100)*int(form.data['quantity'])*duar.harga
            else:
                price = int(form.data['quantity'])*duar.harga
        else:
            form = TransaksiForm(request.POST)
            form_coupon = CouponForm(request.POST)
            kupon = form_coupon.data['coupon']
            price = int(form.data['quantity'])
            for obj in disk:
                if kupon == str(obj.discount_percentage):
                    cek = True
                    kupon_used = obj
                    print(kupon_used)
            if cek:
                if duar.harga >= 1000*kupon_used.minimum_price_in_rupiah_multiplied_by_1000:
                    price = (1 - kupon_used.discount_percentage/100)*int(form.data['quantity'])*duar.harga
                    print('TES KUPON USED', kupon_used.discount_percentage)
                    print('TES QUANTITY', int(form.data['quantity']))
                    if kupon_used.expired_date > make_aware(datetime.now()):
                        price = (1 - kupon_used.discount_percentage/100)*int(form.data['quantity'])*duar.harga
                print('MASUK 1 dengan harga', price)
            else:
                price = int(form.data['quantity'])*duar.harga
                print('MASUK 2 dengan harga', price)

        if form.is_valid():
            obj = Transaksi(
            pembeli = form.data['pembeli'],
            barang = duar.nama,
            kupon = form_coupon.data['coupon'],
            total_harga = price,
            tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d")
            )
            obj.save()
        return redirect('/transaksi/histori/')

    json_data = get_data(disk)
    my_dict = {
        'form':obj,
        'coupon_form':coupon_form,
        'option_form':option_form,
        'duar':duar,
        'json_data':json_data
    }
    return render(request,'transaksi.html',context=my_dict)