from django.urls import path
from .views import transaksi,histori_transaksi,checkout,generate


urlpatterns = [
    path('<int:id_barang>/',transaksi,name='trans'),
    path('histori/',histori_transaksi,name='recent-trans'),
    path('checkout/',checkout),
    path('generate/',generate)
]