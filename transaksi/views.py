from django.shortcuts import render, HttpResponseRedirect, redirect
from django.http import JsonResponse, HttpResponseForbidden, HttpResponse
from django.utils import timezone
from django.utils.timezone import make_aware
from django.contrib.auth.decorators import login_required

from datetime import datetime 
import random
import json
import requests

from .forms import TransaksiForm,CouponForm,OptionCouponForm
from .models import Transaksi

from catalogues.models import Barang

# from coupon.views import get_data
from coupon.models import coupon_data
# from coupon.forms import SearchCouponForm, OptionCouponForm

# Create your views here.
# Duar
def generate(request): #Generate Dummy JSON from Cart
    data_json = []
    cart = Barang.objects.all() #Karena dummy doang
    data_json.clear()
    for item in cart:
        data = {}
        data['id'] = item.id
        data['nama'] = item.nama
        data['tanggal_beli'] = timezone.localtime(timezone.now()).strftime("%Y-%m-%d")
        data['harga'] = item.harga
        data['stok'] = item.stok
        data['qty'] = random.randint(1,5)
        data['image'] = "https://i.imgur.com/vhcUP8n.png" #Samain dulu
        data_json.append(data)
    return JsonResponse(data_json,safe=False)

@login_required
def checkout(request):
    if request.method == "GET":
        option_form = OptionCouponForm() ##
        coupon_form = CouponForm() ##
        disk = coupon_data.objects.all() #object kupon diskon
        json_data = get_data(disk)
        context = {
        'coupon_form':coupon_form,
        'option_form':option_form,
        'json_data' : json_data
        }
        return render(request,"checkout.html",context=context)
    elif request.method == "POST": 
    #Request POST | Dia submit terus cek stoknya gak kosong (di Js) terus bikin model dari data ajax POST JS
        print("masuk")
        data1 = request.POST.get('duar')
        print(data1)
        #terima data dari js

        #buat ke models pake looping

        #redirect ke histori transaksi
        return HttpResponseRedirect('/transaksi/histori/')

def histori_transaksi(request):
    histori = Transaksi.objects.all().order_by('-id')
    #print(histori)
    db = {
        'trans':histori
    }
    return render(request,'histori.html',context=db)

def get_data(coupon_object):
    dict1 = {"code": [], "discount": [], "combo": []}
    for obj in coupon_object:
        dict1["combo"].append({obj.coupon_code:obj.discount_percentage})
        dict1["code"].append(obj.coupon_code)
        dict1["discount"].append(str(obj.discount_percentage))
    return json.dumps(dict1)

def transaksi(request,id_barang):
    my_dict={}
    price = 0
    stok = False
    kupon = 0
    cek = False
    obj = TransaksiForm() #Form of the Transaction
    duar = Barang.objects.get(pk=id_barang) #object barang
    disk = coupon_data.objects.all() #object kupon diskon
    if request.method == "POST":
        form = TransaksiForm(request.POST)
        a = form.data['coupon']
        qty = int(form.data['quantity'])
        kupon = form.data['coupon']
        ############## Section For Stock ##################
        if qty > duar.stok:
            stok = True
            my_dict = {
                'form':obj,
                'duar':duar,
                'isNotReady': stok
            }
            return render(request,'transaksi.html',context=my_dict)
        ###################################################
        ############### Section For Coupon#################
        for i in disk:
            if a == i.coupon_code:
                cek = True
                kupon_used = i
        ####################################################
        #################### Calculation ###################
        if cek and not stok:
            if duar.harga >= 1000*kupon_used.minimum_price_in_rupiah_multiplied_by_1000:
                price = (1 - kupon_used.discount_percentage/100)*int(form.data['quantity'])*duar.harga
                if kupon_used.expired_date > make_aware(datetime.now()):
                    price = (1 - kupon_used.discount_percentage/100)*int(form.data['quantity'])*duar.harga
                duar.stok -= qty
        else:
            price = int(form.data['quantity'])*duar.harga
            duar.stok -= qty
        duar.save()
        ###################################################### 
        if form.is_valid():
            obj = Transaksi(
            pembeli = form.data['pembeli'],
            barang = duar.nama,
            kupon = form.data['coupon'],
            total_harga = price,
            tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d")
            )
            obj.save()
        return HttpResponseRedirect('/transaksi/histori/')
    my_dict = {
        'form':obj,
        'duar':duar,
        'isNotReady':stok
    }
    return render(request,'transaksi.html',context=my_dict)

