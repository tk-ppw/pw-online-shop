var readyArr = [];
var CouponCode = "";
var CouponDisc = 1;
var dataFinal = [] // for JSON to models

var counter = 0;
$(document).ready(e => {
    // e.preventDefault();
    if (counter == 0) { // Prevent Firing AJaX 2 times.
        checkOUT();
    };
    counter++;
    $('#finish-trans').on('click', e => { //Not finished yet
        // e.preventDefault();
        var csrftoken = $('[name="csrfmiddlewaretoken"]').attr('value');
        console.log(dataFinal)
        $.ajax({
            type: "POST",
            url: "/transaksi/checkout/",
            data: {'duar' : dataFinal},
            dataType: 'json',
            headers: {
                "X-CSRFToken": csrftoken,
            },
            success: e => {
                window.location = "/transaksi/checkout/";
            }
        });
    });
    // e.stopPropagation()
    // Bikin implementasi kalo transaksi di finish, nge-clear cart calme + clear variabel di atas
    
    });

function checkOUT() {
    // console.log("func checkOUT!");
    $.ajax({
        type: "GET",
        url:"/transaksi/generate/", //Ini nerima dari Shopping Cart Calme
        success: json => {
            //console.log("checkout AJaX in");
            console.log(json)
            let worthTransaksi = 0;
            let finalTransaksi = worthTransaksi;
            json.forEach(obj => {
                //Logic perhitungan total transaksi buat disini
                obj.total = obj.harga * obj.qty
                console.log(obj)
                $("#transaction-content").append(
                    $("<div/>").addClass('card flex-row flex-wrap').append(
                        $("<div/>").addClass('card-header border-1').css('background-color', 'white').append(
                            $("<img/>").addClass('card img').attr('src', obj.image).css('width','150px').css('height','150px')
                        )
                    ).append(
                        $("<div/>").addClass('card-block px-2').append(
                            $('<h4/>').addClass('card-title').html(obj.nama)).append(
                                $('<p/>').addClass('card-text').html("Quantity : " +  obj.qty + " piece(s)").append(
                                    $('<p/>').addClass('card-text').html("Price per item : Rp. " +  obj.harga)
                                    ).append(
                                        $('<p/>').addClass('card-text').html("Status : " + checkStok(obj.qty,obj.stok))
                                        ).append(
                                            $('<p/>').addClass('card-text').html("Total transaksi : Rp. " + obj.total)
                                            ).append($('<br>'))
                            )
                        )
                ).append(
                    $('<br/>')
                    )
                worthTransaksi += obj.total
                dataFinal.push({
                    'id' : obj.id,
                    'nama' : obj.nama,
                    'image' : obj.image,
                    'qty' : obj.qty,
                    'harga' : obj.harga,
                    'total' : obj.total
                })
            });
            finalTransaksi = worthTransaksi; //before discount
            // Kupon jangan lupa
            // Append seluruh total transaksi buat di sum
            $('#result').html("Total Transaksi Keseluruhan : Rp. <label id=price>" + finalTransaksi + "</label>")
            // Buat Button (disabled) disini
            flagButton();
            // dataFinal = JSON.parse(dataFinal)
        }
    });
};

function checkStok(qty,stok) {
    // console.log("function checkStok!");
    qty = parseInt(qty);
    stok = parseInt(stok);
    if (qty > stok) {
        readyArr.push(true);
        return "Out of Stock."
    } else {
        readyArr.push(false);
        return "Ready Stock!"
    }
};

function flagButton() {
    // console.log("function flagButton!")
    let disable = 0;
    for (let i = 0; i < readyArr.length;i++) {
        if (readyArr[i]) {
            disable++;
            break;
        }
    }
    if (disable != 0) {
        $('#finish-trans').attr('disabled',true); // Mastiin gak bisa di finish kalo ada yang barangnya qty > stock
    }
    readyArr = [];
};
