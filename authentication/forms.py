from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class ModifiedRegistrationForm(UserCreationForm):
    email = forms.EmailField(required = True)

    # hold anything that isnt a form field (meta data)
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')

    def save(self, commit = True):
        # save(commit = T/F) means save or dont save yet
        # calls the super method of this class (UserCreationForm) and calling it's save method with input as false
        user = super(ModifiedRegistrationForm, self).save(commit = False)
        # cleaned_data makes things save to put in data base
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user