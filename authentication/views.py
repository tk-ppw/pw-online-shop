from django.shortcuts import render, redirect
from django.contrib import messages

from .forms import ModifiedRegistrationForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout


# Create your views here.
def login_views(request):
    '''
    fungsi buat login
    '''
    if request.user.is_authenticated:
        return redirect('/')

    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('landing:landing')
    else:
        form = AuthenticationForm()

    return render(request, 'authentication/login.html', {'form':form})


def sign_up_views(request):
    if request.user.is_authenticated:
        return redirect('/')

    if request.method == 'POST':
        '''
        fungsi buat sign up
        '''
        form = ModifiedRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('authentication:login')
    else:
        form = ModifiedRegistrationForm()

    return render(request, 'authentication/signup.html', {'form':form})


def logout_views(request):
    '''
    fungsi buat logout
    '''
    logout(request)
    return redirect('/')
