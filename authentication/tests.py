from django.test import TestCase, override_settings, Client, LiveServerTestCase
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from .apps import AuthenticationConfig
from .views import login_views, sign_up_views, logout_views
from django.contrib.auth.models import User
from catalogues.models import Barang


# Create your tests here.
@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class AuthenticationUnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.barang = Barang.objects.create(nama='mabumabu', harga=100000, stok=10, deskripsi='sjsjsjsjsss')
        self.user = User.objects.create(username='badibos',
                                        first_name='badi',
                                        last_name='bos',
                                        email='badibos@gmail.com',
                                        password='ehrahasiadong'
                                    )

    def test_apps(self):
        self.assertEqual(AuthenticationConfig.name, 'authentication')

    '''
    test if url is exist
    '''
    def test_login_url_is_exist(self):
        response = self.client.get('/auth/login/')
        self.assertEqual(response.status_code, 200)

    def test_signup_url_is_exist(self):
        response = self.client.get('/auth/signup/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_is_exist(self):
        response = self.client.get('/auth/logout/')
        self.assertEqual(response.status_code, 302)

    '''
    test if the urls are using the right function
    '''
    def test_login_using_the_right_function(self):
        found = resolve('/auth/login/')
        self.assertEqual(found.func, login_views)

    def test_signup_using_the_right_function(self):
        found = resolve('/auth/signup/')
        self.assertEqual(found.func, sign_up_views)

    def test_logout_using_the_right_function(self):
        found = resolve('/auth/logout/')
        self.assertEqual(found.func, logout_views)

    '''
    test using post method on both login & signup
    '''
    @override_settings(DEBUG=True)
    def test_logging_in(self):
        response = self.client.post("/auth/login/", follow=True, data={
                        "username": "badibos",
                        "password": "ehrahasiadong"
                    })

        # self.assertRedirects(response, '/', status_code=200,
        #                      target_status_code=301, fetch_redirect_response=True)
        # ini gamau dah soalnya QueryDoesntExist gt barangnya blm ditambahinnnn
        # self.assertContains(response, 'Hot Toys™ Endgame Edition')

    def test_signing_up(self):
        response = self.client.post("/auth/signup/", follow=True, data={
                        "username": "ameng",
                        "first_name": "haji",
                        "last_name": "ameng",
                        "email": "hajiameng@gmail.com",
                        "password1": "sobari",
                        "password2": "sobari"
                    })

        count = User.objects.count()
        # self.assertRedirects(response, '/auth/login/', status_code=301,
        #                      target_status_code=301, fetch_redirect_response=True)
        self.assertEqual(count, 1)

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class AuthenticationFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(
            chrome_options=chrome_options, executable_path='./chromedriver')

    def tearDown(self):
        self.driver.quit()
        super().tearDown()

    def test_auth_functionality_using_selenium(self):
        self.driver.get(self.live_server_url + '/auth/signup/')

        '''
        Signing In
        '''
        time.sleep(5)
        self.driver.find_element_by_id("id_username").send_keys("badibadibadi")
        time.sleep(5)
        self.driver.find_element_by_id("id_first_name").send_keys("badibadi")
        time.sleep(5)
        self.driver.find_element_by_id("id_last_name").send_keys("badi")
        time.sleep(5)
        self.driver.find_element_by_id("id_email").send_keys("badibadibadi@gmail.com")
        time.sleep(5)
        self.driver.find_element_by_id("id_password1").send_keys("ehrahasiadong")
        time.sleep(5)
        self.driver.find_element_by_id("id_password2").send_keys("ehrahasiadong")
        time.sleep(5)
        self.driver.find_element_by_name("signup").click()
        time.sleep(5)
        self.assertIn('login', self.driver.page_source)
        time.sleep(5)

        '''
        Logging In
        '''
        self.driver.find_element_by_id("id_username").send_keys("badibadibadi")
        time.sleep(5)
        self.driver.find_element_by_id("id_password").send_keys("ehrahasiadong")
        time.sleep(5)
        self.driver.find_element_by_name("login").click()
        time.sleep(5)
        # ini gamau dah soalnya QueryDoesn'tExist gt barangnya blm ditambahinnnn
        # self.assertIn('Hot Toys™ Endgame Edition', self.driver.page_source)
        self.assertEquals(self.driver.current_url, self.live_server_url + '/')
