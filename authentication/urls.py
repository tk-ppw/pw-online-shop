import django.contrib.auth
from django.urls import path, include
from . import views

app_name = 'authentication'

urlpatterns = [
    path('login/', views.login_views, name='login'),
    path('signup/', views.sign_up_views, name='signup'),
    path('logout/', views.logout_views, name='logout'),
]