from django.test import TestCase, override_settings, Client
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .apps import ReviewConfig
from .views import reviewPerProduct
from .models import Review
from .forms import ReviewForm
from catalogues.models import Barang

# class unittest(TestCase):
    # def test_review_page(self):
    #     response = Client().get('review/*')
    #     self.assertEqual(response.status_code, 200)

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class ReviewTest(TestCase):
    # SetUp Test
    def setUp(self):
        self.client = Client()
        self.barang = Barang.objects.create(nama='mabumabu', harga=100000, stok=10, deskripsi='sjsjsjsjsss')
        self.review = Review.objects.create(
            review_product=self.barang,
            review_name='sarmiji',
            review_city='DEPOKCITTY',
            review_star=5,
            review_review='HUAHUAHUHAUHAUA',
        )
    # Test Apps
    def test_apps(self):
        self.assertEqual(ReviewConfig.name, 'review')
        # self.assertEqual(review.get_app_config('review').name, 'review')
    # Test URL, Function
    def test_url_review(self):
        barang1 = self.barang
        review1 = self.review
        url_slug = barang1.nama
        response = self.client.get('/review/' + str(url_slug) + '/')
        self.assertEqual(response.status_code, 200)

    def test_post_and_render_the_result(self):
        barang1 = self.barang
        review1 = self.review
        url_slug = barang1.nama
        response_post = self.client.post('/review/' + str(url_slug) + '/')
        self.assertEqual(response_post.status_code, 200)

    def test_url_form_post_review(self):
        form = ReviewForm()
        form_data = {'review_name': 'Bading','review_city': "Depok City of Chaos",'review_review': "PPW anjaying" ,'review_star': 4}
        form = ReviewForm(data=form_data)
        self.assertTrue(form.is_valid(), True)

    # ijin nambahin dhi biar cepe
    def test_review_post_actual_data(self):
        barang1 = self.barang
        review1 = self.review
        url_slug = barang1.nama
        response = self.client.post('/review/' + str(url_slug) + '/', follow=True, data={
                        "review_name": "badibos",
                        "review_city": "ehrahasiadong",
                        "review_review": "eh malooone",
                        "review_star": 5
                    })

        # self.assertRedirects(response, '/auth/welcome/badibos', status_code=301,
        #                      target_status_code=301, fetch_redirect_response=True)
        self.assertContains(response, 'badibos')

    def test_func_review(self):
        barang1 = self.barang
        review1 = self.review
        url_slug = barang1.nama
        found = resolve('/review/' + str(url_slug) + '/')
        self.assertEqual(found.func, reviewPerProduct)

    def test_templ_review(self):
        barang1 = self.barang
        review1 = self.review
        url_slug = barang1.nama
        response = self.client.post('/review/' + str(url_slug) + '/')
        self.assertTemplateUsed(response, 'review.html')

    # Model testing
    def test_model_review(self):
        self.assertEqual(str(self.review), "Review {} by {}".format(self.review.review_product, self.review.review_name))

    # def test_event_page_is_exist(self):
    #     response = self.client.get('/event/')
    #     self.assertEqual(response.status_code, 200)

    # def test_event_page_using_view_func(self):
    #     found = resolve('/event/')
    #     self.assertEqual(found.func, events)

    # def test_event_page_post_success_and_render_the_result(self):
    #     response_post = self.client.post(
    #         '/event/', {
    #             'nama_event': 'Mabok',
    #             'deskripsi': 'asikasikan'
    #         }
    #     )
    #     self.assertEqual(response_post.status_code, 200)

    # def test_event_add_guest_page_post_success_and_render_the_result(self):
    #     response_post = self.client.post(
    #         '/event/add/Narkoba', {
    #             'guest': 'badingsyalala'
    #         }
    #     )
    #     self.assertEqual(response_post.status_code, 404)
    # def test_event_page_add_event_guests(self):
    #     new_act = Review.objects.create(nama_event="mabumabu")
    #     new_act.save()
    #     response = self.client.post('/event/add/mabumabu',{'guest':"buset"})
    #     self.assertEqual(response.status_code, 302)