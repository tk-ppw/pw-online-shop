from django import forms
from . import models

starChoices = (
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
)

class ReviewForm(forms.ModelForm):
    # review_product = models.ForeignKey(Barang, on_delete=models.CASCADE)
    # review_name = forms.CharField(label='Name',widget=forms.TextInput(
    #     attrs={
    #         'class' : 'form-control mx-auto',
    #         'placeholder' : "write the subject's description...",
    #         'style' : 'width: 50%;'
    #     }, 
    # ))

    
    # review_city = forms.CharField(label='City/Town',widget=forms.TextInput(
    #     attrs={
    #         'class' : 'form-control mx-auto',
    #         'placeholder' : "write the subject's description...",
    #         'style' : 'width: 50%;'
    #     }, 
    # ))

    
    # review_review = forms.Textarea(label='Review',widget=forms.Textarea(
    #     attrs={
    #         'class' : 'form-control mx-auto',
    #         'placeholder' : "write the subject's description...",
    #         'style' : 'width: 50%;'
    #     }, 
    # ))

    
    # review_star = forms.SelectMultiple(label='Star', choices=starChoices, widget=forms.SelectMultiple(
    #     choices=starChoices,
    #     attrs={
    #         'class' : 'form-control mx-auto',
    #         'placeholder' : "write the subject's description...",
    #         'style' : 'width: 50%;'
    #     }, 
    # ))

    

    class Meta:
        model = models.Review
        fields = (
            'review_name',
            'review_city',
            'review_review',
            'review_star',
        )