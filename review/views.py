from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.http import HttpResponseRedirect

from .models import Review
from catalogues.models import Barang
from .forms import ReviewForm

# Create your views here.
def reviewPerProduct(request, product_name):
    product = get_object_or_404(Barang, nama=product_name)
    review_per_product = Review.objects.filter(review_product=product)

    if request.method == 'POST':
        review_form = ReviewForm(request.POST)
        if review_form.is_valid():
            new_review = review_form.save(commit=False)
            new_review.review_product = product
            new_review.save()
            review_form = ReviewForm()
            return HttpResponseRedirect(request.path_info)

    else:
        review_form = ReviewForm()

    context = {
        'product' : product,
        'review_per_product' : review_per_product,
        'review_form' : review_form,
    }

    return render(request, 'review.html', context)