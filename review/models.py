from django.db import models
from catalogues.models import Barang

# Create your models here.

# Bikin db models u/ review
# consists of:
#> Name
#> City/Town
#> Review

# BARANG nanti diapus trs merge punya calme

# class Barang(models.Model):
#     # jenisbarang_kategori = models.ForeignKey(Kategori, on_delete=models.CASCADE)
#     namabarang = models.CharField(max_length=50, unique=True)
#     # kuponbarang_kupon = models.ForeignKey(Kupon, on_delete=models.CASCADE) => ini gua bingung cm keknya foreignkey
#     hargabarang = models.IntegerField()
#     stokbarang = models.IntegerField()
#     deskripsibarang = models.TextField(max_length=500)



starChoices = (
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
)

class Review(models.Model):
    review_product = models.ForeignKey(Barang, on_delete=models.CASCADE)
    review_name = models.CharField(max_length=50)
    review_city = models.CharField(max_length=20)
    review_star = models.IntegerField(choices=starChoices, default=5)
    review_review = models.TextField(max_length=400)
    review_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Review {} by {}".format(self.review_product, self.review_name)



