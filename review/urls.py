
from django.urls import path
from . import views

urlpatterns = [
    # path('', views.reviewPerProduct ,name='review'),
    path('<str:product_name>/', views.reviewPerProduct ,name='review'),
]