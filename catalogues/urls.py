from django.urls import path
from . import views

#app_name = 'catalogues'

urlpatterns = [
    path('', views.catalogues, name='catalogues'),
    path('details/<int:pk>/', views.details, name='details'),
    #path('cart/', views.cart_detail, name='cart')
    # dilanjutkan ...
]