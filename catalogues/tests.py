from django.test import TestCase, Client, override_settings
from django.urls import resolve
from .models import *
from .views import *

# Create your tests here.
@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class TestCatalogue(TestCase):
    def test_catalogue_page(self):
        response = Client().get('/catalogues/')
        self.assertEqual(response.status_code, 200)
    
    def test_func_page(self):
        found = resolve('/catalogues/')
        self.assertEqual(found.func, catalogues)

    def test_html_page(self):
        response = Client().get('/catalogues/')
        self.assertTemplateUsed(response, 'catalogues.html')

    def test_details_page(self):
        duar = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)  
        barang_1 = Barang.objects.get(pk=duar.id)
        response = Client().get('/catalogues/details/' + str(barang_1.id) + '/')
        self.assertEqual(response.status_code, 200)
    
    def test_views(self):
        orang = Client()
        tes = Barang.objects.all().count()
        response = orang.get('/catalogues/details/<int:tes>')
        self.assertIsNotNone(response)


