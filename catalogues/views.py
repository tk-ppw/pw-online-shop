from django.shortcuts import Http404, get_object_or_404, redirect, render
from .models import Barang

# Create your views here.

def catalogues(request):
    item = Barang.objects.all()
    db = {
        'kunci' : item
    }
    return render(request, 'catalogues.html', {'kunci' : item})

def details(request, pk):
    item = Barang.objects.get(pk=pk)
    db = {
        'kunci' : item,
    }
    return render(request, 'details.html', {'kunci' : item})


