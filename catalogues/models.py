from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Barang(models.Model):
    nama = models.CharField(max_length=120)
    harga = models.IntegerField('harga')
    stok = models.IntegerField('stok')
    deskripsi = models.CharField(max_length=120)

    def __str__(self):
        return self.nama
