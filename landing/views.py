from django.shortcuts import render,HttpResponse
from catalogues.models import Barang

# Create your views here.
def landing(request):
    barang = Barang.objects.all()

    barang1 = Barang.objects.get(id=1)

    context = {
        'barang1' : barang1,
    }
    return render(request,'landingpage.html', context)
