from django.test import TestCase, override_settings, Client
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .apps import LandingConfig
from .views import landing
from catalogues.models import Barang


# Create your tests here.
@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class TestLanding(TestCase):
    def test_apps(self):
        self.assertEqual(LandingConfig.name, 'landing')

    def test_url_landing(self):
        barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_view_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)


